export const getParams = (argv: string[]) => {
  const [_node, _file, pathName, ...methods] = argv;
  const name = pathName.split("/").reverse()[0];

  return { methods, pathName, name };
};
