import { toClassName, toMethodName } from "./names";

describe("toClassName", () => {
  test("snake case", () => {
    const expected = "TestingClass";
    const subject = "testing-class";
    expect(toClassName(subject)).toEqual(expected);
  });
});

describe("toMethodName", () => {
  test("snake case", () => {
    const expected = "methodName";
    const subject = "Method-name";
    expect(toMethodName(subject)).toEqual(expected);
  });
});
