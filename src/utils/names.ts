import * as _ from "lodash";

export const toClassName = className => _.upperFirst(_.camelCase(className));
export const toMethodName = methodName => _.lowerFirst(_.camelCase(methodName));
