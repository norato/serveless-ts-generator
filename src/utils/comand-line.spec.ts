import { getParams } from "./comand-line";
import { methodName, methodName2 } from "../partials/controller/mocks";

describe("getParams", () => {
  test("should ", () => {
    const className = "testing-class";
    const response = getParams([
      "node",
      "file",
      className,
      methodName,
      methodName2
    ]);
    expect(response.name).toEqual(className);
    expect(response.methods).toEqual([methodName, methodName2]);
  });
});
