import { joinMethods } from "./method";
import { toClassName } from "../../utils/names";

export const classTarget = "CLASS_NAME";
export const endClass = "}\n";

export const classNameTemplate = "export class CLASS_NAMEController {";

export const createClassDeclaration = className =>
  classNameTemplate.replace(classTarget, toClassName(className));

export const constrolerTemplate = (className, methods) =>
  [createClassDeclaration(className), joinMethods(methods), endClass].join(
    "\n"
  );

export const createController = (className, methods) => ({
  fileName: "controller.ts",
  src: constrolerTemplate(className, methods)
});
