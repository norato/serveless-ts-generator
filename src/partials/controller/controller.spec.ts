import { constrolerTemplate } from "./controller";
import {
  expectedMethod,
  expectedMethod2,
  methodName,
  methodName2
} from "./mocks";

const className = "Testing";

const expectClass = `export class ${className}Controller {\n${expectedMethod}\n${expectedMethod2}\n}\n`;

describe("Controller", () => {
  test("should ", () => {
    expect(constrolerTemplate(className, [methodName, methodName2])).toEqual(
      expectClass
    );
  });
});
