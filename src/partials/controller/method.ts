import { toMethodName } from "../../utils/names";

export const methodTemplate = `
\tpublic METHOD_NAME(req, res, next): Promise<string> {
\t\tconst response = res.json('METHOD_NAME');
\t\treturn Promise.resolve(response);
\t}`;

export const target = new RegExp(/METHOD_NAME/, "g");

export const createMethodImplementation = (methodName: string): string =>
  methodTemplate.replace(target, toMethodName(methodName));

export const joinMethods = (methodNames: string[]): string =>
  methodNames.map(createMethodImplementation).join(`\n`);
