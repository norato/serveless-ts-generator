export const expectedMethod = `
\tpublic testingMethodName(req, res, next): Promise<string> {
\t\tconst response = res.json('testingMethodName');
\t\treturn Promise.resolve(response);
\t}`;

export const expectedMethod2 = `
\tpublic testingMethodName2(req, res, next): Promise<string> {
\t\tconst response = res.json('testingMethodName2');
\t\treturn Promise.resolve(response);
\t}`;

export const methodName = "testingMethodName";
export const methodName2 = "testingMethodName2";
