import { createMethodImplementation, joinMethods } from "./method";
import {
  methodName,
  expectedMethod,
  expectedMethod2,
  methodName2
} from "./mocks";

describe("createMethodImplementation", () => {
  test("should ", () => {
    expect(createMethodImplementation(methodName)).toEqual(expectedMethod);
  });
});

describe("pushMethods", () => {
  test("should ", () => {
    const methods = `${expectedMethod}\n${expectedMethod2}`;
    expect(joinMethods([methodName, methodName2])).toEqual(methods);
  });
});
