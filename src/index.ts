import { getParams } from "./utils/comand-line";
import * as fs from "fs";
import * as path from "path";
import * as fsExtra from "fs-extra";
import { createController } from "./partials/controller/controller";

const { name, methods, pathName } = getParams(process.argv);

async function foobar() {
  try {
    const pathExists = await fsExtra.pathExists(pathName);
    if (!pathExists) {
      await fsExtra.mkdirp(pathName);
    }
    const controller = createController(name, methods);
    const controllerPath = path.resolve(pathName, controller.fileName);

    fs.writeFileSync(controllerPath, controller.src, "utf-8");
  } catch (error) {
    console.error(error);
  }
}
foobar();
